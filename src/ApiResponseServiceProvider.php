<?php

namespace APP3Null\ApiResponseMacros;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ApiResponseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Success Response
        Response::macro('success', function ($data = [], $code = 200) {
            return Response::json($data)->setStatusCode($code);
        });

        // Error Response
        Response::macro('fail', function ($code, $message) {
            return Response::json([
                'code' => $code,
                'message' => $message
            ], 400);
        });
    }
}
